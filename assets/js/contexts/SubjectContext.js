import React, {createContext} from 'react';
import axios from "axios";
import SubjectTable from "../components/SubjectTable";

export const SubjectContext = createContext();

class SubjectContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            subjects: []
        };
        this.readSubject();
    }

    // read
    readSubject() {
        axios.get('/api/subject')
            .then(response => {
                console.log(response.data)
                this.setState({
                    subjects: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    render() {
        return (
            <SubjectContext.Provider value={{
                ...this.state,
                readSubject: this.readSubject.bind(this)
            }}>
                {this.props.children}
            </SubjectContext.Provider>
        );
    }
}

export default SubjectContextProvider;