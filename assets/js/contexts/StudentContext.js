import React, {createContext} from 'react';
import axios from "axios";

export const StudentContext = createContext();
const qs = require('querystring');


class StudentContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            students: [],
            bootstrapHide: " d-none",
            schools: [],
            gradeSections: [
                // TODO create API to fetch avaliable student:enum gradesection
                {value: 'a',},
                {value: 'b',},
                {value: 'c',},
                {value: 'd',},
                {value: 'e',},
                {value: 'f',},
            ],
            grades: [],
            locations: [],

            formFirstName: "",
            formLastName: "",
            formOIB: "",
            formDOB: "",
            formAddress: "",
            formLocation: "0",
            formSchool: "0",
            formGrade: "0",
            formGradeSection: "a",
            formID: "0",
        };
        this.readStudent();
        this.readGrades();
        this.readLocations();
        this.readSchools();
    }

    handleFirstNameChange = (event) => {
        this.setState({
            formFirstName: event.target.value
        })
    }
    handleLastNameChange = (event) => {
        this.setState({
            formLastName: event.target.value
        })
    }
    handleOIBChange = (event) => {
        this.setState({
            formOIB: event.target.value
        })
    }
    handleDOBChange = (event) => {
        this.setState({
            formDOB: event.target.value
        })
    }
    handleAddressChange = (event) => {
        this.setState({
            formAddress: event.target.value
        })
    }
    handleLocationChange = (event) => {
        this.setState({
            formLocation: event.target.value
        })
    }
    handleSchoolChange = (event) => {
        this.setState({
            formSchool: event.target.value
        })
    }
    handleGradeChange = (event) => {
        this.setState({
            formGrade: event.target.value
        })
    }
    handleGradeSectionChange = (event) => {
        this.setState({
            formGradeSection: event.target.value
        })
    }
    handleIDChange = (event) => {
        this.setState({
            formID: event.target.value
        })
    }


    // create
    createStudent(student) {
        axios.post('/api/student', qs.stringify(student), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(response => {
                this.setState({
                    message: "User added",
                    formFirstName: "",
                    formLastName: "",
                    formOIB: "",
                    formDOB: "",
                    formAddress: "",
                    formLocation: "0",
                    formSchool: "0",
                    formGrade: "0",
                    formGradeSection: "a",
                    formID: "0",
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // read
    readStudent() {
        axios.get('/api/student')
            .then(response => {
                this.setState({
                    students: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // update
    updateStudent(student, id) {
        console.log("update student:" + id)
        axios.put('/api/student/' + id, qs.stringify(student), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(response => {
                console.log(response.data)
                this.setState({
                    // students: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // delete
    deleteStudent(id) {
        console.log("deleteing...studentid" + id);
        axios.delete('/api/student/' + id)
            .then(response => {
                console.log(response.data)
            }).catch(error => {
            console.error(error.message)
        })
    }

    // read grades
    readGrades() {
        axios.get('/api/grade')
            .then(response => {
                this.setState({
                    grades: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // read locations
    readLocations() {
        axios.get('/api/location')
            .then(response => {
                this.setState({
                    locations: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // read schools
    readSchools() {
        axios.get('/api/school')
            .then(response => {
                this.setState({
                    schools: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    // read single student for edit
    readSingleStudent(id) {
        // or fetch from students with filter
        // let currentStudent = this.props.students.filter(element => element.id.includes(id));
        // console.log(currentStudent);

        axios.get('/api/student/' + id)
            .then(response => {
                console.log(response.data)
                this.setState({
                    formFirstName: response.data.name,
                    formLastName: response.data.surname,
                    formOIB: response.data.OIB,
                    formDOB: response.data.dateOfBirth.substring(0, 10),
                    formAddress: response.data.address,
                    formLocation: response.data.postalCode.postalCode,
                    formSchool: response.data.school.id,
                    formGrade: response.data.grade.grade,
                    formGradeSection: response.data.gradeSection,
                    formID: response.data.id,
                })
            }).catch(error => {
            console.error(error)
        })
    }

    render() {
        return (
            <StudentContext.Provider value={{
                ...this.state,
                createStudent: this.createStudent.bind(this),
                readStudent: this.readStudent.bind(this),
                updateStudent: this.updateStudent.bind(this),
                deleteStudent: this.deleteStudent.bind(this),
                readSingleStudent: this.readSingleStudent.bind(this),

                handleFirstNameChange: this.handleFirstNameChange,
                handleLastNameChange: this.handleLastNameChange,
                handleOIBChange: this.handleOIBChange,
                handleAddressChange: this.handleAddressChange,
                handleSchoolChange: this.handleSchoolChange,
                handleGradeChange: this.handleGradeChange,
                handleGradeSectionChange: this.handleGradeSectionChange,
                handleLocationChange: this.handleLocationChange,
                handleDOBChange: this.handleDOBChange,
                handleIDChange: this.handleIDChange,

            }}>
                {this.props.children}
            </StudentContext.Provider>
        );
    }
}

export default StudentContextProvider;