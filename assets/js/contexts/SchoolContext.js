import React, {createContext} from 'react';
import axios from "axios";

export const SchoolContext = createContext();

class SchoolContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            schools: []
        };
        this.readSchool();
    }

    // read
    readSchool() {
        axios.get('/api/school')
            .then(response => {
                this.setState({
                    schools: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    render() {
        return (
            <SchoolContext.Provider value={{
                ...this.state,
                readSchool: this.readSchool.bind(this)
            }}>
                {this.props.children}
            </SchoolContext.Provider>
        );
    }
}

export default SchoolContextProvider;