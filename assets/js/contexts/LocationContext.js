import React, {createContext} from 'react';
import axios from "axios";

export const LocationContext = createContext();

class LocationContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            locations: []
        };
        this.readLocation();
    }

    // read
    readLocation() {
        axios.get('/api/location')
            .then(response => {
                console.log(response.data)
                this.setState({
                    locations: response.data
                })
            }).catch(error => {
            console.error(error)
        })
    }

    render() {
        return (
            <LocationContext.Provider value={{
                ...this.state,
                readLocation: this.readLocation.bind(this)
            }}>
                {this.props.children}
            </LocationContext.Provider>
        );
    }
}

export default LocationContextProvider;