import React, {useContext} from 'react';
import {StudentContext} from "../contexts/StudentContext";

function StudentTable() {
    const context = useContext(StudentContext);

    function handleClick(id, e) {
        e.preventDefault();
        if (window.confirm("This will delete student and all of his grades! \nDo You want to continue?")) {
            context.deleteStudent(id);
        }
    }

    function handleLoadStudent(id, e) {
        e.preventDefault();
        context.readSingleStudent(id);
    }

    // function handleClickForm(e) {
    //     // TODO check why class is not updating
    //     e.preventDefault();
    //     context.bootstrapHide = "mt-5";
    // }

    return (
        // TODO create pagination
        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
                <th scope="col">Grade</th>
                <th scope="col">School</th>
                <th scope="col">OIB</th>
                <th className={'text-center'} scope="col">
                    Operations<br/>
                    <a href={'#'} onClick={(event => handleClickForm(event))}>Create new</a>
                </th>
            </tr>
            </thead>
            <tbody>
            {context.students.map((student, index) => (
                <tr key={'student' + index}>
                    <td>{student.id}</td>
                    <td>{student.name}</td>
                    <td>{student.surname}</td>
                    <td>{student.grade.grade} {student.gradeSection}</td>
                    <td>{student.school.name}</td>
                    <td>{student.OIB}</td>
                    <td className={'text-center'}>
                        <a href={'#'} onClick={(event => handleLoadStudent(student.id, event))}>
                            <svg className="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z"/>
                                <path fillRule="evenodd"
                                      d="M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z"
                                      clipRule="evenodd"/>
                            </svg>
                        </a>
                        &nbsp;&nbsp;
                        <a href={'/api/student/' + student.id} onClick={(event => handleClick(student.id, event))}>
                            <svg className="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z"/>
                                <path fillRule="evenodd"
                                      d="M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                                      clipRule="evenodd"/>
                            </svg>
                        </a>
                    </td>
                </tr>
            ))}


            </tbody>
        </table>
    );
}

export default StudentTable;