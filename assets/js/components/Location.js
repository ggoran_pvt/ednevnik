import React, {Component} from "react";
import LocationContextProvider from "../contexts/LocationContext";
import LocationTable from "./LocationTable";


export class Location extends Component {
    render() {
        return (
            <div className={"container"}>
                <LocationContextProvider>
                    <LocationTable></LocationTable>
                </LocationContextProvider>
            </div>

        )
            ;
    }
}