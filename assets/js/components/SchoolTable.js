import React, {useContext} from 'react';
import {SchoolContext} from "../contexts/SchoolContext";


function SchoolTable() {
    const context = useContext(SchoolContext);

    return (

        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Location</th>
                <th scope="col">Postal Code</th>
                <th scope="col">School Code</th>
                <th scope="col">Download stats</th>
            </tr>
            </thead>
            <tbody>
            {context.schools.map((school, index) => (
                <tr key={'school' + index}>
                    <td>{school.id}</td>
                    <td>{school.name}</td>
                    <td>{school.postalCode.name}</td>
                    <td>{school.postalCode.postalCode}</td>
                    <td>{school.code}</td>
                    <td><a href={'/api/school/' + school.id + '/stats'}>Download</a></td>
                </tr>
            ))}
            </tbody>
        </table>
    );
}

export default SchoolTable;