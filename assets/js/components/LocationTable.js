import React, {useContext} from 'react';
import {LocationContext} from "../contexts/LocationContext";



function LocationTable() {
    const context = useContext(LocationContext);

    return (

        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">PostalCode</th>
            </tr>
            </thead>
            <tbody>
            {context.locations.map((location, index) => (
                <tr key={'location' + index}>
                    <td>{location.id}</td>
                    <td>{location.name}</td>
                    <td>{location.postalCode}</td>
                </tr>
            ))}


            </tbody>
        </table>
    );
}

export default LocationTable;