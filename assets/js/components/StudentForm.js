import React, {useContext, useRef} from 'react';
import {StudentContext} from "../contexts/StudentContext";


function StudentForm() {
    const context = useContext(StudentContext);

    function handleFormSubmit(e) {
        e.preventDefault();
        const student = {
            name: context.formFirstName,
            surname: context.formLastName,
            postalCode: context.formLocation,
            school: context.formSchool,
            grade: context.formGrade,
            oib: context.formOIB,
            address: context.formAddress,
            dateOfBirth: context.formDOB,
            gradeSection: context.formGradeSection,
            studentID: context.formID
        };
        if (context.formID === "0") {
            context.createStudent(student, context);
        } else {
            context.updateStudent(student, context.formID, context)
        }

    }

    return (
        <div className="mt-5">
            <div className="form-header">Form</div>
            <form className="mt-3">
                <div className="form-row">
                    <div className="col">
                        <input type="text" className="form-control" placeholder="First name" id="firstName"
                               value={context.formFirstName} onChange={context.handleFirstNameChange}/>
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Last name"
                               value={context.formLastName} onChange={context.handleLastNameChange}/>
                    </div>
                    <div className="col">
                        <input className="form-control" type="number" placeholder="OIB"
                               value={context.formOIB} onChange={context.handleOIBChange}/>
                    </div>
                </div>

                <div className="form-row mt-3">
                    <div className="col">
                        <input className="form-control" type="date" placeholder="Date of birth"
                               value={context.formDOB} onChange={context.handleDOBChange}/>
                    </div>
                    <div className="col">
                        <input type="text" className="form-control" placeholder="Address"
                               value={context.formAddress} onChange={context.handleAddressChange}/>
                    </div>
                    <div className="col">
                        <select className="form-control mr-sm-2" id="location"
                                value={context.formLocation} onChange={context.handleLocationChange}>

                            <option disabled={true} value="0">Choose Location...</option>
                            {context.locations.map((location, index) => (
                                <option key={'location' + index} value={location.postalCode}>{location.name}</option>
                            ))}
                        </select>
                    </div>
                </div>

                <div className="form-row mt-3">
                    <div className="col">
                        <select className="form-control mr-sm-2" id="school"
                                value={context.formSchool} onChange={context.handleSchoolChange}>
                            <option disabled={true} value="0">Choose school...</option>

                            {context.schools.map((school, index) => (
                                <option key={'school' + index} value={school.id}>{school.name}</option>
                            ))}

                        </select>
                    </div>
                    <div className="col">
                        <select className="form-control mr-sm-2" id="grade"
                                onChange={context.handleGradeChange} value={context.formGrade}>
                            <option disabled={true}>Choose grade...
                            </option>

                            {context.grades.map((grade, index) => (
                                <option key={'school' + index} value={grade.id}>{grade.grade}</option>
                            ))}

                        </select>
                    </div>
                    <div className="col">
                        <select className="form-control mr-sm-2" id="gradeSection"
                                value={context.formGradeSection}
                                onChange={context.handleGradeSectionChange}>
                            <option disabled={true}>Choose grade section...
                            </option>

                            {context.gradeSections.map((gradeSection, index) => (
                                <option key={'school' + index} value={gradeSection.value}>{gradeSection.value}</option>
                            ))}

                        </select>
                    </div>
                </div>

                <input type="text" className="form-control d-none" value={context.formID}
                       onChange={context.handleIDChange}/>


                <div className={'mt-3 mb-5 float-right'}>
                    <button type="submit" className="btn btn-primary" onClick={(event => handleFormSubmit(event))}> Go
                    </button>
                </div>

            </form>
        </div>
    );
}

export default StudentForm;