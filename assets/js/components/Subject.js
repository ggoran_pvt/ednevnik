import React, {Component} from "react";
import SubjectContextProvider from "../contexts/SubjectContext";
import SubjectTable from "./SubjectTable";


export class Subject extends Component {
    render() {
        return (
            <div className={"container"}>
                <SubjectContextProvider>
                    <SubjectTable></SubjectTable>
                </SubjectContextProvider>
            </div>

        )
            ;
    }
}