import React, {Component} from "react";
import SchoolContextProvider from "../contexts/SchoolContext";
import SchoolTable from "./SchoolTable";


export class School extends Component {
    render() {
        return (
            <div className={"container"}>
                <SchoolContextProvider>
                    <SchoolTable></SchoolTable>
                </SchoolContextProvider>
            </div>

        )
            ;
    }
}