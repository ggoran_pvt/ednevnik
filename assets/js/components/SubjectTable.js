import React, {useContext} from 'react';
import {SubjectContext} from "../contexts/SubjectContext";


function SubjectTable() {
    const context = useContext(SubjectContext);

    return (

        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Class</th>
                <th scope="col">Code</th>
            </tr>
            </thead>
            <tbody>
            {context.subjects.map((subject, index) => (
                <tr key={'subject' + index}>
                    <td>{subject.id}</td>
                    <td>{subject.name}</td>
                    <td>Grade {subject.grade.grade}</td>
                    <td>{subject.code}</td>
                </tr>
            ))}


            </tbody>
        </table>
    );
}

export default SubjectTable;