import React, {Component} from "react";
import StudentContextProvider from "../contexts/StudentContext";
import StudentForm from "./StudentForm";
import StudentTable from "./StudentTable";


export class Student extends Component {
    render() {
        return (
            <div className={"container"}>
                <StudentContextProvider>
                    <StudentForm></StudentForm>
                    <StudentTable></StudentTable>
                </StudentContextProvider>
            </div>

        )
            ;
    }
}