import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';

import {Location} from "./components/Location";
import {School} from "./components/School";
import {Subject} from "./components/Subject";
import {Student} from "./components/Student";


const $ = require('jquery');
require('bootstrap');

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className={"container"}>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <div className={"collapse navbar-collapse"}>
                            <div className={"navbar-nav"}>
                                <NavLink className={"nav-item nav-link"} exact activeClassName="active"
                                         to="/">Location</NavLink>
                                <NavLink className={"nav-item nav-link"} exact activeClassName="active"
                                         to="/schools">Schools</NavLink>
                                <NavLink className={"nav-item nav-link"} exact activeClassName="active"
                                         to="/subjects">Subjects</NavLink>
                                <NavLink className={"nav-item nav-link"} exact activeClassName="active"
                                         to="/students">Students</NavLink>
                            </div>
                        </div>
                    </nav>

                    <Switch>
                        <Route path="/schools" component={School}/>
                        <Route path="/subjects" component={Subject}/>
                        <Route path="/students" component={Student}/>
                        <Route path="/" component={Location}/>
                    </Switch>
                </div>
            </BrowserRouter>
        )
            ;
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));