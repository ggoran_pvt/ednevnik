<?php


namespace App\Controller;


use App\Entity\Score;
use App\Entity\Subject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ScoreController extends AbstractController
{
    /**
     * @Route("/api/score", name="scores", methods={"GET"})
     *
     * TODO this might not be needed
     */
    public function getScore()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Score::class);
        $result = $repository->findAll();
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['scores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }

    /**
     * @Route("/api/score/{score}", name="score", methods={"GET"})
     * TODO this might not be needed
     */
    public function findSubject($score)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Score::class);
        $result = $repository->findOneBy(array('id' => $score));
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['scores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );

    }


    /**
     * @Route("/api/subjectsStats", name="subjectsStats", methods={"GET"})
     *
     * @return Response - all scores from all subjects (grouped by subject)
     */
    public function getSubjectStatistics()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Subject::class);
        $result = $repository->findAll();
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }
}