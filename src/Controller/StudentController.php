<?php


namespace App\Controller;

use App\Entity\Grade;
use App\Entity\Location;
use App\Entity\School;
use App\Entity\Student;
use Doctrine\ORM\ORMException;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\This;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class StudentController extends AbstractController
{
    /**
     * @Route("/api/student", name="student", methods={"GET"})
     * @return Response - every student info with all scores, location, school, grade information
     */
    public function getStudents()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Student::class);
        $result = $repository->findAll();

        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::ENABLE_MAX_DEPTH => false,
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student', 'allScores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }

    /**
     * @Route("/api/student/school/{schoolID}", name="getStudentGradeAndAverage", methods={"GET"})
     * @param $schoolID - id of school
     * @return Response - all students of single school with student info, all scores and average
     */
    public function getStudentGradeAndAverage($schoolID)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Student::class);
        $result = $repository->findByExampleField($schoolID);


        foreach ($result as $key => $student) {
            $students[$key]['id'] = $student->getId();
            $students[$key]['firstName'] = $student->getName();
            $students[$key]['lastName'] = $student->getSurname();
            $students[$key]['gradeSection'] = $student->getGradeSection();
            $students[$key]['grade'] = $student->getGrade()->getGrade();


            if (count($student->getScores())) {
                foreach ($student->getScores() as $score) {
                    $students[$key]['scores'][] = $score->getScore();
                }
                $students[$key]['average'] = array_sum($students[$key]['scores']) / count($students[$key]['scores']);
            } else {
                $students[$key]['scores'][] = "No score yet";
                $students[$key]['average'] = "No score yet";
            }
        }


        return $this->json($students, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student', 'allScores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }

    /**
     * @Route("/api/student/{student}", name="studentSingle", methods={"GET"})
     * @param $student - student ID
     * @return Response - every student info with all scores, location, school, grade information
     */
    public function findStudent($student)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Student::class);
        $result = $repository->findOneBy(array('id' => $student));

        return $this->json($result, Response::HTTP_OK, [], [
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => false,
                ObjectNormalizer::CIRCULAR_REFERENCE_LIMIT => 1,
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student', 'allScores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object;
                }]
        );

    }


    /**
     * @Route("/api/student", name="createStudent", methods={"POST"})
     * create student
     * @param Request $request - params of student
     * depending on
     *  - School ID
     *  - postal code
     *  - grade ID
     *
     * TODO create additional verification
     */
    public function createStudent(Request $request)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $postalCode = $request->get('postalCode');
        $school = $request->get('school');
        $grade = $request->get('grade');
        $oib = $request->get('oib');
        $address = $request->get('address');
        $dateOfBirth = $request->get('dateOfBirth');
        $gradeSection = $request->get('gradeSection');

        // fetch necessary objects
        $locationRepository = $this->getDoctrine()->getManager()->getRepository(Location::class);
        $postalCode = $locationRepository->findOneBy(array('postal_code' => $postalCode));

        $schoolRepository = $this->getDoctrine()->getManager()->getRepository(School::class);
        $school = $schoolRepository->findOneBy(array('id' => $school));

        $gradeRepository = $this->getDoctrine()->getManager()->getRepository(Grade::class);
        $grade = $gradeRepository->findOneBy(array('id' => $grade));

        $entityManager = $this->getDoctrine()->getManager();
        $student = new Student();
        $student->setName($name);
        $student->setSurname($surname);
        $student->setPostalCode($postalCode);
        $student->setSchool($school);
        $student->setGrade($grade);
        $student->setOIB($oib);
        $student->setAddress($address);
        $student->setDateOfBirth(new \DateTime($dateOfBirth));
        $student->setGradeSection($gradeSection);

        $entityManager->persist($student);
        $entityManager->flush();

        return $this->json($student, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );

    }

    /**
     * @Route("/api/student/{id}", name="updateStudent", methods={"PUT"})
     * @param $id - ID of student that needs to be updated
     * @param Request $request - params of student
     * depending on
     *  - School ID
     *  - postal code
     *  - grade ID
     *
     * TODO remove duplicates in new function
     */
    public function updateStudent(Request $request, $id)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $postalCode = $request->get('postalCode');
        $school = $request->get('school');
        $grade = $request->get('grade');
        $oib = $request->get('oib');
        $address = $request->get('address');
        $dateOfBirth = $request->get('dateOfBirth');
        $gradeSection = $request->get('gradeSection');

        // fetch necessary objects
        $locationRepository = $this->getDoctrine()->getManager()->getRepository(Location::class);
        $postalCode = $locationRepository->findOneBy(array('postal_code' => $postalCode));

        $schoolRepository = $this->getDoctrine()->getManager()->getRepository(School::class);
        $school = $schoolRepository->findOneBy(array('id' => $school));

        $gradeRepository = $this->getDoctrine()->getManager()->getRepository(Grade::class);
        $grade = $gradeRepository->findOneBy(array('id' => $grade));

        $entityManager = $this->getDoctrine()->getManager();
        $student = $entityManager->getRepository(Student::class)->find($id);


        $student->setName($name);
        $student->setSurname($surname);
        $student->setPostalCode($postalCode);
        $student->setSchool($school);
        $student->setGrade($grade);
        $student->setOIB($oib);
        $student->setAddress($address);
        $student->setDateOfBirth(new \DateTime($dateOfBirth));
        $student->setGradeSection($gradeSection);

        $entityManager->persist($student);
        $entityManager->flush();


        return $this->json($student, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );

    }


    /**
     * @Route("/api/student/{id}", name="deleteStudent", methods={"DELETE"})
     * @param $id - student id what need to be deleted
     */
    public function deleteStudent($id)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $student = $entityManager->getRepository(Student::class)->find($id);
            $entityManager->remove($student);
            $entityManager->flush();

            return $this->json("Student deleted");
        } catch (\Exception $exception) {
            return $this->json($exception->getMessage());
        }

    }


    /**
     * @Route("/api/studentsInClass/{schoolId}", name="studentClass", methods={"GET"})
     * @param $schoolId - school ID that need to be analysed
     * @return Response - get class information ( grade + grade section, list of all students in class)
     */
    public function getStutendsPerClass($schoolId)
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        $serverName = $protocol . "://" . $_SERVER['SERVER_NAME'];

        // TODO - check alternative way to fetch same data without repeating code, repository?
        $client = HttpClient::create();
        $response = $client->request('GET', $serverName . '/api/student/school/' . $schoolId);
        $contentJson = $response->getContent();
        $content = json_decode($contentJson, true);

        // get necessary data
        foreach ($content as $key => $student) {
            $students[$key]['id'] = $student['id'];
            $students[$key]['firstName'] = $student['firstName'];
            $students[$key]['lastName'] = $student['lastName'];
            $students[$key]['average'] = $student['average'];
            $students[$key]['grade'] = $student['grade'];
            $students[$key]['gradeSection'] = $student['gradeSection'];
        }

        // sort by grade
        $grade = array_column($students, 'grade');
        $gradeSection = array_column($students, 'gradeSection');
        array_multisort($grade, SORT_ASC, $gradeSection, SORT_ASC, $students);

        $previousClass = '';
        foreach ($students as $student) {
            $currentClass = $student['grade'] . $student['gradeSection'];
            // create sheet names
            if ($previousClass != $currentClass) {
                $sheet[$currentClass]['name'] = $currentClass;
                $previousClass = $currentClass;
            }
            $sheet[$currentClass]['students'][] = $student;
        }

        return $this->json($sheet);


    }

}