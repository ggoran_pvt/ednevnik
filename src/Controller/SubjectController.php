<?php


namespace App\Controller;


use App\Entity\Subject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SubjectController extends AbstractController
{
    /**
     * @Route("/api/subject", name="subjects", methods={"GET"})
     *
     * @return Response - return list of all subjects and associated grade
     */
    public function getSubjects()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Subject::class);
        $result = $repository->findAll();
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student', 'allScores'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }

    /**
     * @Route("/api/subject/{subject}", name="subject", methods={"GET"})
     * @param $subject - id of subject
     * @return Response - list of subject and associated scores
     */
    public function findSubject($subject)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Subject::class);
        $result = $repository->findOneBy(array('id' => $subject));
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student', 'scores', 'grade'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );

    }


    /**
     * @Route("/api/subjectsStats", name="subjectsStats", methods={"GET"})
     * @return Response - return all subject, grade and scores for all subjects
     */
    public function getSubjectStatistics()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Subject::class);
        $result = $repository->findAll();
        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['student'],
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }
}