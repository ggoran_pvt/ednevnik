<?php

namespace App\Controller;

use App\Entity\Grade;
use App\Entity\Location;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LocationController extends AbstractController
{
    /**
     * @Route("/api/location", name="locations", methods={"GET"})
     * @return Response - list of all available locations
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Location::class);
        $result = $repository->findAll();

        return $this->json($result);
    }


    /**
     * @Route("/api/location/{location}", name="location", methods={"GET"})
     * @@param $location - input value postal code or name accepted
     * @return Response - specific location information
     */
    public function findLocation($location)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Location::class);
        $result = $repository->findOneBy(array('name' => $location));
        if (!$result) {
            $result = $repository->findOneBy(array('postal_code' => $location));
        }

        if ($result == null) {
            $result = "Sorry $location not found";
        }

        return $this->json($result);
    }

}
