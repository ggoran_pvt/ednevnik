<?php

namespace App\Controller;

use App\Entity\Grade;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class GradeController extends AbstractController
{
    /**
     * @Route("/api/grade", name="grades", methods={"GET"})
     * @return Response - list of all grades
     */
    public function getGrades()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Grade::class);
        $result = $repository->findAll();

        return $this->json($result);
    }

    /**
     * @Route("/api/grade/{grade}", name="grade", methods={"GET"})
     * @param $grade - grade number 1-8
     * @return Response - find specific grade
     */
    public function findGrade($grade)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(Grade::class);
        $result = $repository->findOneBy(array('grade' => $grade));
        return $this->json($result);
    }
}
