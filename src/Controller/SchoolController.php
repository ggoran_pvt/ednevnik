<?php

namespace App\Controller;

use App\Entity\School;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SchoolController extends AbstractController
{
    /**
     * @Route("/api/school", name="getSchool", methods={"GET"})
     * @return Response - all schools
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(School::class);
        $result = $repository->findAll();

        return $this->json($result, Response::HTTP_OK, [], [
                ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }]
        );
    }

    /**
     * @Route("/api/school/{school}", name="findSchool", methods={"GET"})
     * @param $school - school name, code or id accepted
     * @return Response - list of all schools + Location
     */
    public function findSchool($school)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(School::class);
        $result = $repository->findOneBy(array('name' => $school));
        if (!$result) {
            $result = $repository->findOneBy(array('id' => $school));
        }
        if (!$result) {
            $result = $repository->findOneBy(array('code' => $school));
        }

        return $this->json($result);
    }

    /**
     * @Route("/api/school/{schoolId}/stats", name="schoolStats", methods={"GET"})
     */
    public function schoolStats($schoolId)
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        $serverName = $protocol . "://" . $_SERVER['SERVER_NAME'];

        // TODO - check alternative way to fetch same data without repeating code, repository?
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', $serverName . '/api/studentsInClass/' . $schoolId);
            $contentJson = $response->getContent();
            $content = json_decode($contentJson, true);

            $spreadsheet = new Spreadsheet();
            foreach ($content as $class) {
                // create new spreadsheet with custom name
                $myWorkSheet = $spreadsheet->createSheet();
                $myWorkSheet->setTitle($class['name']);
                $activeSheet = $spreadsheet->setActiveSheetIndexByName($class['name']);

                // create entry in each row
                $studentCounter = 0;
                foreach ($class['students'] as $student) {
                    $studentCounter++;
                    $activeSheet->setCellValue('A' . $studentCounter, $student['id']);
                    $activeSheet->setCellValue('B' . $studentCounter, $student['firstName']);
                    $activeSheet->setCellValue('C' . $studentCounter, $student['lastName']);
                    $activeSheet->setCellValue('D' . $studentCounter, $student['average']);
                }
                // class average
                $rowCount = $studentCounter + 2;
                $activeSheet->setCellValue('C' . $rowCount, "Class average");
                $activeSheet->setCellValue('D' . $rowCount, "=AVERAGE(D1:D$studentCounter)");
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(12);
            }

            // delete starting sheet
            $spreadsheet->removeSheetByIndex(0);

            // set header for excel xlsx file
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename=' . 'school.xlsx');

            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');

            return $this->json("File Downloaded");
        } catch (\Exception $exception) {
            return $this->json($exception->getMessage());
        }
    }

}
