<?php

namespace App\Command;

use App\Entity\Location;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UpdateLocationCommand extends Command
{
    protected static $defaultName = 'app:update-location';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Fetch location and update DB location table based on XML result')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // get locations from posta.hr
        $client = HttpClient::create();

        $response = $client->request('GET', 'https://www.posta.hr/postanskiurediRh_rv.aspx?vrsta=xml');
        try {
            if ($response->getStatusCode()) {
                // OK
                $data = $response->getContent();
                if (substr( $data, 0, 5 ) !== "<?xml"){
                    // not valid format
                    die($io->error("Looks like this is not XML"));
                }
            }
        } catch (TransportExceptionInterface $e) {
            // handle error
            die($io->error($e->getMessage()));
        }

        // fix closing tag issue
        $data = str_replace('</P>', '</p>', $data);

        // serializer to convert XML to array
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->decode($data, 'xml');
        $data = $data["ured"];

        // remove duplicates
        foreach ($data as $row) {
            if (!isset($locationApI[$row['grad']])) {
                $locationApI[$row['grad']] = $row['brojPu'];
            }
        }


        // fetch location from DB
        $entityManager = $this->container->get('doctrine')->getManager();
//        $entityManager = $this->getDoctrine()->getManager();
        $location = $entityManager->getRepository(Location::class)->findAll();

        foreach ($location as $entry) {
            $locationDB[$entry->getName()] = $entry->getPostalCode();
        }
        if (empty($locationDB)) {
            $locationDB = [];
        }

        // compare arrays
        $locationsToSave = array_diff_key($locationApI, $locationDB);


        // save to DB
        foreach ($locationsToSave as $location => $postalCode) {
            $item = new Location();
            $item->setName($location);
            $item->setPostalCode($postalCode);
            $entityManager->persist($item);
        }
        $entityManager->flush();

        $io->success('Success, check DB');

        return 0;
    }
}
