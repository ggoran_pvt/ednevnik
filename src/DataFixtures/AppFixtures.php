<?php

namespace App\DataFixtures;

use App\Entity\Grade;
use App\Entity\School;
use App\Entity\Score;
use App\Entity\Student;
use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }


    public function load(ObjectManager $entityManager)
    {
        // START of Grade fixtures
        for ($i = 1; $i <= 8; $i++) {
            $grade = new Grade();
            $grade->setGrade($i);
            $this->addReference("grade$i", $grade);
            $entityManager->persist($grade);
        }
        $entityManager->flush();
        // END of Grade fixtures

        // START of Subjects
        // START of grade1
        $hrv1 = new Subject();
        $hrv1->setCode('hrv1');
        $hrv1->setName('HRVATSKI JEZIK');
        $hrv1->setGrade($this->getReference("grade1"));
        $entityManager->persist($hrv1);

        $lik1 = new Subject();
        $lik1->setCode('lik1');
        $lik1->setName('LIKOVNA KULTURA');
        $lik1->setGrade($this->getReference("grade1"));
        $entityManager->persist($lik1);

        $gla1 = new Subject();
        $gla1->setCode('gla1');
        $gla1->setName('GLAZBENA KULTURA');
        $gla1->setGrade($this->getReference("grade1"));
        $entityManager->persist($gla1);

        $eng1 = new Subject();
        $eng1->setCode('eng1');
        $eng1->setName('ENGLESKI JEZIK');
        $eng1->setGrade($this->getReference("grade1"));
        $entityManager->persist($eng1);

        $mat1 = new Subject();
        $mat1->setCode('mat1');
        $mat1->setName('MATEMATIKA');
        $mat1->setGrade($this->getReference("grade1"));
        $entityManager->persist($mat1);

        $pid1 = new Subject();
        $pid1->setCode('pid1');
        $pid1->setName('PRIRODA I DRUŠTVO');
        $pid1->setGrade($this->getReference("grade1"));
        $entityManager->persist($pid1);

        $tzk1 = new Subject();
        $tzk1->setCode('tzk1');
        $tzk1->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk1->setGrade($this->getReference("grade1"));
        $entityManager->persist($tzk1);
        //end of grade1

        //start of grade2
        $hrv2 = new Subject();
        $hrv2->setCode('hrv2');
        $hrv2->setName('HRVATSKI JEZIK');
        $hrv2->setGrade($this->getReference("grade2"));
        $entityManager->persist($hrv2);

        $lik2 = new Subject();
        $lik2->setCode('lik2');
        $lik2->setName('LIKOVNA KULTURA');
        $lik2->setGrade($this->getReference("grade2"));
        $entityManager->persist($lik2);

        $gla2 = new Subject();
        $gla2->setCode('gla2');
        $gla2->setName('GLAZBENA KULTURA');
        $gla2->setGrade($this->getReference("grade2"));
        $entityManager->persist($gla2);

        $eng2 = new Subject();
        $eng2->setCode('eng2');
        $eng2->setName('ENGLESKI JEZIK');
        $eng2->setGrade($this->getReference("grade2"));
        $entityManager->persist($eng2);

        $mat2 = new Subject();
        $mat2->setCode('mat2');
        $mat2->setName('MATEMATIKA');
        $mat2->setGrade($this->getReference("grade2"));
        $entityManager->persist($mat2);

        $pid2 = new Subject();
        $pid2->setCode('pid2');
        $pid2->setName('PRIRODA I DRUŠTVO');
        $pid2->setGrade($this->getReference("grade2"));
        $entityManager->persist($pid2);

        $tzk2 = new Subject();
        $tzk2->setCode('tzk2');
        $tzk2->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk2->setGrade($this->getReference("grade2"));
        $entityManager->persist($tzk2);
        //end of grade2

        //start of grade3
        $hrv3 = new Subject();
        $hrv3->setCode('hrv3');
        $hrv3->setName('HRVATSKI JEZIK');
        $hrv3->setGrade($this->getReference("grade3"));
        $entityManager->persist($hrv3);

        $lik3 = new Subject();
        $lik3->setCode('lik3');
        $lik3->setName('LIKOVNA KULTURA');
        $lik3->setGrade($this->getReference("grade3"));
        $entityManager->persist($lik3);

        $gla3 = new Subject();
        $gla3->setCode('gla3');
        $gla3->setName('GLAZBENA KULTURA');
        $gla3->setGrade($this->getReference("grade3"));
        $entityManager->persist($gla3);

        $eng3 = new Subject();
        $eng3->setCode('eng3');
        $eng3->setName('ENGLESKI JEZIK');
        $eng3->setGrade($this->getReference("grade3"));
        $entityManager->persist($eng3);

        $mat3 = new Subject();
        $mat3->setCode('mat3');
        $mat3->setName('MATEMATIKA');
        $mat3->setGrade($this->getReference("grade3"));
        $entityManager->persist($mat3);

        $pid3 = new Subject();
        $pid3->setCode('pid3');
        $pid3->setName('PRIRODA I DRUŠTVO');
        $pid3->setGrade($this->getReference("grade3"));
        $entityManager->persist($pid3);

        $tzk3 = new Subject();
        $tzk3->setCode('tzk3');
        $tzk3->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk3->setGrade($this->getReference("grade3"));
        $entityManager->persist($tzk3);
        //end of grade3

        //start of grade4
        $hrv4 = new Subject();
        $hrv4->setCode('hrv4');
        $hrv4->setName('HRVATSKI JEZIK');
        $hrv4->setGrade($this->getReference("grade4"));
        $entityManager->persist($hrv4);

        $lik4 = new Subject();
        $lik4->setCode('lik4');
        $lik4->setName('LIKOVNA KULTURA');
        $lik4->setGrade($this->getReference("grade4"));
        $entityManager->persist($lik4);

        $gla4 = new Subject();
        $gla4->setCode('gla4');
        $gla4->setName('GLAZBENA KULTURA');
        $gla4->setGrade($this->getReference("grade4"));
        $entityManager->persist($gla4);

        $eng4 = new Subject();
        $eng4->setCode('eng4');
        $eng4->setName('ENGLESKI JEZIK');
        $eng4->setGrade($this->getReference("grade4"));
        $entityManager->persist($eng4);

        $mat4 = new Subject();
        $mat4->setCode('mat4');
        $mat4->setName('MATEMATIKA');
        $mat4->setGrade($this->getReference("grade4"));
        $entityManager->persist($mat4);

        $pid4 = new Subject();
        $pid4->setCode('pid4');
        $pid4->setName('PRIRODA I DRUŠTVO');
        $pid4->setGrade($this->getReference("grade4"));
        $entityManager->persist($pid4);

        $tzk4 = new Subject();
        $tzk4->setCode('tzk4');
        $tzk4->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk4->setGrade($this->getReference("grade4"));
        $entityManager->persist($tzk4);
        //end of grade4

        //start of grade5
        $hrv5 = new Subject();
        $hrv5->setCode('hrv5');
        $hrv5->setName('HRVATSKI JEZIK');
        $hrv5->setGrade($this->getReference("grade5"));
        $entityManager->persist($hrv5);

        $lik5 = new Subject();
        $lik5->setCode('lik5');
        $lik5->setName('LIKOVNA KULTURA');
        $lik5->setGrade($this->getReference("grade5"));
        $entityManager->persist($lik5);

        $gla5 = new Subject();
        $gla5->setCode('gla5');
        $gla5->setName('GLAZBENA KULTURA');
        $gla5->setGrade($this->getReference("grade5"));
        $entityManager->persist($gla5);

        $eng5 = new Subject();
        $eng5->setCode('eng5');
        $eng5->setName('ENGLESKI JEZIK');
        $eng5->setGrade($this->getReference("grade5"));
        $entityManager->persist($eng5);

        $mat5 = new Subject();
        $mat5->setCode('mat5');
        $mat5->setName('MATEMATIKA');
        $mat5->setGrade($this->getReference("grade5"));
        $entityManager->persist($mat5);

        $pid5 = new Subject();
        $pid5->setCode('pid5');
        $pid5->setName('PRIRODA');
        $pid5->setGrade($this->getReference("grade5"));
        $entityManager->persist($pid5);

        $geo5 = new Subject();
        $geo5->setCode('geo5');
        $geo5->setName('GEOGRAFIJA');
        $geo5->setGrade($this->getReference("grade5"));
        $entityManager->persist($geo5);

        $pov5 = new Subject();
        $pov5->setCode('pov5');
        $pov5->setName('POVIJEST');
        $pov5->setGrade($this->getReference("grade5"));
        $entityManager->persist($pov5);

        $tzk5 = new Subject();
        $tzk5->setCode('tzk5');
        $tzk5->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk5->setGrade($this->getReference("grade5"));
        $entityManager->persist($tzk5);

        $teh5 = new Subject();
        $teh5->setCode('teh5');
        $teh5->setName('TEHNIČKA KULTURA');
        $teh5->setGrade($this->getReference("grade5"));
        $entityManager->persist($teh5);

        $inf5 = new Subject();
        $inf5->setCode('inf5');
        $inf5->setName('INFORMATIKA');
        $inf5->setGrade($this->getReference("grade5"));
        $entityManager->persist($inf5);
        //end of grade5

        //start of grade6
        $hrv6 = new Subject();
        $hrv6->setCode('hrv6');
        $hrv6->setName('HRVATSKI JEZIK');
        $hrv6->setGrade($this->getReference("grade6"));
        $entityManager->persist($hrv6);

        $lik6 = new Subject();
        $lik6->setCode('lik6');
        $lik6->setName('LIKOVNA KULTURA');
        $lik6->setGrade($this->getReference("grade6"));
        $entityManager->persist($lik6);

        $gla6 = new Subject();
        $gla6->setCode('gla6');
        $gla6->setName('GLAZBENA KULTURA');
        $gla6->setGrade($this->getReference("grade6"));
        $entityManager->persist($gla6);

        $eng6 = new Subject();
        $eng6->setCode('eng6');
        $eng6->setName('ENGLESKI JEZIK');
        $eng6->setGrade($this->getReference("grade6"));
        $entityManager->persist($eng6);

        $mat6 = new Subject();
        $mat6->setCode('mat6');
        $mat6->setName('MATEMATIKA');
        $mat6->setGrade($this->getReference("grade6"));
        $entityManager->persist($mat6);

        $pid6 = new Subject();
        $pid6->setCode('pid6');
        $pid6->setName('PRIRODA');
        $pid6->setGrade($this->getReference("grade6"));
        $entityManager->persist($pid6);

        $geo6 = new Subject();
        $geo6->setCode('geo6');
        $geo6->setName('GEOGRAFIJA');
        $geo6->setGrade($this->getReference("grade6"));
        $entityManager->persist($geo6);

        $pov6 = new Subject();
        $pov6->setCode('pov6');
        $pov6->setName('POVIJEST');
        $pov6->setGrade($this->getReference("grade6"));
        $entityManager->persist($pov6);

        $tzk6 = new Subject();
        $tzk6->setCode('tzk6');
        $tzk6->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk6->setGrade($this->getReference("grade6"));
        $entityManager->persist($tzk6);

        $teh6 = new Subject();
        $teh6->setCode('teh6');
        $teh6->setName('TEHNIČKA KULTURA');
        $teh6->setGrade($this->getReference("grade6"));
        $entityManager->persist($teh6);

        $inf6 = new Subject();
        $inf6->setCode('inf');
        $inf6->setName('INFORMATIKA');
        $inf6->setGrade($this->getReference("grade6"));
        $entityManager->persist($inf6);
        //end of grade6

        //start of grade7
        $hrv7 = new Subject();
        $hrv7->setCode('hrv7');
        $hrv7->setName('HRVATSKI JEZIK');
        $hrv7->setGrade($this->getReference("grade7"));
        $entityManager->persist($hrv7);

        $lik7 = new Subject();
        $lik7->setCode('lik7');
        $lik7->setName('LIKOVNA KULTURA');
        $lik7->setGrade($this->getReference("grade7"));
        $entityManager->persist($lik7);

        $gla7 = new Subject();
        $gla7->setCode('gla7');
        $gla7->setName('GLAZBENA KULTURA');
        $gla7->setGrade($this->getReference("grade7"));
        $entityManager->persist($gla7);

        $eng7 = new Subject();
        $eng7->setCode('eng7');
        $eng7->setName('ENGLESKI JEZIK');
        $eng7->setGrade($this->getReference("grade7"));
        $entityManager->persist($eng7);

        $mat7 = new Subject();
        $mat7->setCode('mat7');
        $mat7->setName('MATEMATIKA');
        $mat7->setGrade($this->getReference("grade7"));
        $entityManager->persist($mat7);

        $pid7 = new Subject();
        $pid7->setCode('pid7');
        $pid7->setName('BIOLOGIJA');
        $pid7->setGrade($this->getReference("grade7"));
        $entityManager->persist($pid7);

        $kem7 = new Subject();
        $kem7->setCode('kem7');
        $kem7->setName('KEMIJA');
        $kem7->setGrade($this->getReference("grade7"));
        $entityManager->persist($kem7);

        $fiz7 = new Subject();
        $fiz7->setCode('fiz7');
        $fiz7->setName('FIZIKA');
        $fiz7->setGrade($this->getReference("grade7"));
        $entityManager->persist($fiz7);

        $geo7 = new Subject();
        $geo7->setCode('geo7');
        $geo7->setName('GEOGRAFIJA');
        $geo7->setGrade($this->getReference("grade7"));
        $entityManager->persist($geo7);

        $pov7 = new Subject();
        $pov7->setCode('pov7');
        $pov7->setName('POVIJEST');
        $pov7->setGrade($this->getReference("grade7"));
        $entityManager->persist($pov7);

        $tzk7 = new Subject();
        $tzk7->setCode('tzk7');
        $tzk7->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk7->setGrade($this->getReference("grade7"));
        $entityManager->persist($tzk7);

        $teh7 = new Subject();
        $teh7->setCode('teh7');
        $teh7->setName('TEHNIČKA KULTURA');
        $teh7->setGrade($this->getReference("grade7"));
        $entityManager->persist($teh7);

        $inf7 = new Subject();
        $inf7->setCode('inf7');
        $inf7->setName('INFORMATIKA');
        $inf7->setGrade($this->getReference("grade7"));
        $entityManager->persist($inf7);
        //end of grade7

        //start of grade8
        $hrv8 = new Subject();
        $hrv8->setCode('hrv8');
        $hrv8->setName('HRVATSKI JEZIK');
        $hrv8->setGrade($this->getReference("grade8"));
        $entityManager->persist($hrv8);

        $lik8 = new Subject();
        $lik8->setCode('lik8');
        $lik8->setName('LIKOVNA KULTURA');
        $lik8->setGrade($this->getReference("grade8"));
        $entityManager->persist($lik8);

        $gla8 = new Subject();
        $gla8->setCode('gla8');
        $gla8->setName('GLAZBENA KULTURA');
        $gla8->setGrade($this->getReference("grade8"));
        $entityManager->persist($gla8);

        $eng8 = new Subject();
        $eng8->setCode('eng8');
        $eng8->setName('ENGLESKI JEZIK');
        $eng8->setGrade($this->getReference("grade8"));
        $entityManager->persist($eng8);

        $mat8 = new Subject();
        $mat8->setCode('mat8');
        $mat8->setName('MATEMATIKA');
        $mat8->setGrade($this->getReference("grade8"));
        $entityManager->persist($mat8);

        $pid8 = new Subject();
        $pid8->setCode('pid8');
        $pid8->setName('BIOLOGIJA');
        $pid8->setGrade($this->getReference("grade8"));
        $entityManager->persist($pid8);

        $kem8 = new Subject();
        $kem8->setCode('kem8');
        $kem8->setName('KEMIJA');
        $kem8->setGrade($this->getReference("grade8"));
        $entityManager->persist($kem8);

        $fiz8 = new Subject();
        $fiz8->setCode('fiz8');
        $fiz8->setName('FIZIKA');
        $fiz8->setGrade($this->getReference("grade8"));
        $entityManager->persist($fiz8);

        $geo8 = new Subject();
        $geo8->setCode('geo8');
        $geo8->setName('GEOGRAFIJA');
        $geo8->setGrade($this->getReference("grade8"));
        $entityManager->persist($geo8);

        $pov8 = new Subject();
        $pov8->setCode('pov8');
        $pov8->setName('POVIJEST');
        $pov8->setGrade($this->getReference("grade8"));
        $entityManager->persist($pov8);

        $tzk8 = new Subject();
        $tzk8->setCode('tzk8');
        $tzk8->setName('TJELESNA I ZDRAVSTVENA KULTURA');
        $tzk8->setGrade($this->getReference("grade8"));
        $entityManager->persist($tzk8);

        $teh8 = new Subject();
        $teh8->setCode('teh8');
        $teh8->setName('TEHNIČKA KULTURA');
        $teh8->setGrade($this->getReference("grade8"));
        $entityManager->persist($teh8);

        $inf8 = new Subject();
        $inf8->setCode('inf8');
        $inf8->setName('INFORMATIKA');
        $inf8->setGrade($this->getReference("grade8"));
        $entityManager->persist($inf8);
        //end of grade8
        $entityManager->flush();
        // END of subjects

        // LOAD postal codes
        $locationsRepository = $entityManager->getRepository('App:Location');
        $locationZagreb = $locationsRepository->findOneBy(array('name' => 'ZAGREB'));
        $locationOsijek = $locationsRepository->findOneBy(array('name' => 'OSIJEK'));
        $locationSplit = $locationsRepository->findOneBy(array('name' => 'SPLIT'));
        $entityManager->flush();
        // end of locations

        // start  school
        $schoolOsijek = new School();
        $schoolOsijek->setCode('skola1');
        $schoolOsijek->setName('F.K.Frankopana');
        $schoolOsijek->setAddress('Frankopanska 4');
        $schoolOsijek->setPostalCode($locationOsijek);
        $entityManager->persist($schoolOsijek);

        $schoolZagreb = new School();
        $schoolZagreb->setCode('skola2');
        $schoolZagreb->setName('Kralja Tomislava Zagreb');
        $schoolZagreb->setAddress('Nova cesta 92');
        $schoolZagreb->setPostalCode($locationZagreb);
        $entityManager->persist($schoolZagreb);

        $schoolSplit = new School();
        $schoolSplit->setCode('skola3');
        $schoolSplit->setName('Gripe Split');
        $schoolSplit->setAddress('Ul. Alojzija Stepinca 12');
        $schoolSplit->setPostalCode($locationSplit);
        $entityManager->persist($schoolSplit);

        $entityManager->flush();
        // end of school

        // start of students
        // populate school Osijek
        for ($j = 1; $j <= 8; $j++) {
            for ($i = 0; $i < 10; $i++) {
                $student = new Student();
                $student->setOIB($this->faker->numberBetween($min = 1111111111, $max = 9999999999));
                $student->setName($this->faker->firstName);
                $student->setSurname($this->faker->lastName);
                $student->setAddress($this->faker->streetName . " " . $this->faker->buildingNumber);
                $student->setPostalCode($locationOsijek);
                $student->setDateOfBirth($this->faker->dateTimeBetween($startDate = '-30 years', $endDate = '-10 years'));
                $student->setSchool($this->faker->randomElement($schoolArray = array($schoolSplit, $schoolOsijek, $schoolZagreb)));
                $student->setGrade($this->getReference("grade$j"));
                $student->setGradeSection($this->faker->randomElement($array = array('a', 'b', 'c', 'd')));
                // avoid duplications $j = grade $i=student number
                $this->addReference("student$j$i", $student);
                $entityManager->persist($student);


                //set grades for students
                for ($z = 0; $z < $this->faker->randomDigit; $z++) {
                    $score = new Score();
                    $score->setStudent($student);
                    // get random subject (graded dependent)
                    // take random subject from DB
                    $subjects = $entityManager->getRepository('App:Subject')->findBy(array('grade' => $this->getReference("grade$j")));
                    //shuffle so I get random subject
                    shuffle($subjects);
                    $score->setSubject($subjects[0]);
                    $score->setDate($this->faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'));
                    $score->setDescription($this->faker->text($maxNbChars = 50));
                    $score->setScore($this->faker->numberBetween($min = 1, $max = 5));
                    $entityManager->persist($score);
                }
            }
        }
        // end of students
        $entityManager->flush();
    }
}
