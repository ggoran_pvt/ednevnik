# eDnevnik

## How to run
- clone project
- install dependencies
- php bin/console doctrine:database:create
- php bin/console doctrine:schema:create
- php bin/console app:update-location
- php bin/console doctrine:fixtures:load --append
- start using app


## Dependencies
-  DB fixer depending on location DB to be correct, so app:update-location first then append with migration

## TODO
- display feedback messages
- frontend 
- improvements on API
- cleanup
- more detailed view (example - click on student or school to show detailed stats)


#### NOTE:
- project is made with more or less pure Symfony/React components
- feel free to comment, and review for improvements and point mistakes =)
